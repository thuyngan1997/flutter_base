import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

//API
const String baseUrl = 'http://10.60.158.89:9005/monitorings/v1';

// color constants
const Color COLOR_MAIN_TEXT = Color(0xFF042D66);
const Color COLOR_LABEL = Color(0xFF7992A3);
//Text constants
const String APP_NAME = 'Water Quality Monitoring';
const String TITLE_ABOUT_PARAMETERS_SCREEN = 'Thông số';
const String TITLE_HOME_SCREEN = 'Trang chủ';
const String TITLE_WARNING_SCREEN = 'Cảnh báo';
const String TITLE_DETAIL_WARNING_SCREEN = 'Cảnh báo chi tiết';
const String TITLE_FEEDBACK_SCREEN = 'Phản hồi';
const String TITLE_PROFILE_SCREEN = 'Cá nhân';
const String TITLE_ACCOUNT_INFORMATION_SCREEN = 'Thông tin cá nhân';
const String TITLE_SETTING_SCREEN = 'Cài đặt';
const String TITLE_HISTORY_SCREEN = 'Lịch sử';
const String TITLE_DETAILED_PARAMETER = 'Thông báo chi tiết';
const String TITLE_CHANGE_PASSWORD = 'Đổi mật khẩu';
const String TITLE_LOGOUT = 'Đăng xuất';
const String TITLE_FOLLOWED_PLACE_SCREEN = 'Yêu thích';
const String TITLE_SUPPORT_SCREEN = 'Hỗ trợ';

//Text constants
const String LABEL_LOGIN_PHONE = 'Số điện thoại';
const String LABEL_LOGIN_PASSWORD = 'Mật khẩu';
const String TEXT_FOOTER = 'Developed by Viettel Group';
const String TEXT_ALERT_FEEDBACK_SUCCESS =
    'Gởi phản hồi thành công!\nCảm ơn bạn đã góp ý để chúng tôi ngày càng hoàn thiện hơn';
const String LABEL_FEEDBACK_SELECT_ICON =
    'Hãy chia sẻ với chúng tôi về \ntrải nghiệm của bạn';
const String LABEL_FEEDBACK_INPUT =
    'Góp ý để chúng tôi \nngày càng hoàn thiện hơn';
const String TEXT_ALERT_CONFIRM_LOGOUT =
    'Bạn chắc chắn muốn đăng xuất khỏi ứng dụng';
//ERROR constants
const String ERROR_VALIDATE_EMPTY_PHONE_LOGIN =
    'Số điện thoại không được để trống';
const String ERROR_INVALID_PHONE_LOGIN = 'Số điện thoại không hợp lệ';
const String ERROR_VALIDATE_EMPTY_PASSWORD_LOGIN =
    'Mật khẩu không được để trống';
const String ERROR_VALIDATE_EMPTY_CURRENT_PASSWORD_LOGIN =
    'Mật khẩu hiện tại không được để trống';
const String ERROR_VALIDATE_EMPTY_NEW_PASSWORD_LOGIN =
    'Mật khẩu mới không được để trống';
const String ERROR_SERVER = 'Không kết nối được với máy chủ';
const String ERROR_VALIDATE_PASSWORD = 'Mật khẩu mới phải là mật khẩu mạnh';
const String ERROR_VALIDATE_EMPTY_FEEDBACK =
    'Nội dung phản hồi không được để trống';
