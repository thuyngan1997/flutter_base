import 'dart:ui';
import 'package:flutter/material.dart';

String getStatus(int value) {
  if (value >= 91 && value <= 100) {
    return "Rất tốt";
  } else if (value >= 76 && value <= 90) {
    return "Tốt";
  } else if (value >= 51 && value <= 75) {
    return "Trung bình";
  } else if (value >= 26 && value <= 50) {
    return "Kém";
  } else if (value >= 10 && value <= 25) {
    return "Ô nhiễm";
  } else if (value < 10) {
    return "Ô nhiễm nặng";
  }
  return "Không xác định";
}

Color getColor(int value) {
  if (value >= 91 && value <= 100) {
    return Color(0xff26A0FF);
  } else if (value >= 76 && value <= 90) {
    return Color(0xff00A651);
  } else if (value >= 51 && value <= 75) {
    return Color(0xffE2BD0F);
  } else if (value >= 26 && value <= 50) {
    return Color(0xffB85800);
  } else if (value >= 10 && value <= 25) {
    return Color(0xffA60000);
  } else if (value < 10) {
    return Color(0xff6C0101);
  }
  return Color(0xff26A0FF);
}

String getIcon(int value) {
  if (value >= 91 && value <= 100) {
    return "/ic_wqi_rattot.png";
  } else if (value >= 76 && value <= 90) {
    return "/ic_wqi_tot.png";
  } else if (value >= 51 && value <= 75) {
    return "/ic_wqi_trungbinh.png";
  } else if (value >= 26 && value <= 50) {
    return "/ic_wqi_kem.png";
  } else if (value >= 10 && value <= 25) {
    return "/ic_wqi_onhiem.png";
  } else if (value < 10) {
    return "/ic_wqi_onhiemnang.png";
  }
  return "/ic_water.png";
}

String getMarkerImage(int value) {
  if (value >= 91 && value <= 100) {
    return "/marker_rattot.png";
  } else if (value >= 76 && value <= 90) {
    return "/marker_tot.png";
  } else if (value >= 51 && value <= 75) {
    return "/marker_trungbinh.png";
  } else if (value >= 26 && value <= 50) {
    return "/marker_kem.png";
  } else if (value >= 10 && value <= 25) {
    return "/marker_onhiemnang.png";
  } else if (value < 10) {
    return "/marker_onhiemratnang.png";
  }
  return null;
}

String getIconParameter(String type) {
  if (type == "wqi") {
    return "/ic_water.png";
  } else if (type == "ph") {
    return "/ic_ph.png";
  } else if (type == "orp") {
    return "/ic_orp.png";
  } else if (type == "turbility") {
    return "/ic_turbility.png";
  } else if (type == "salinity") {
    return "/ic_salinity.png";
  } else if (type == "temperature") {
    return "/ic_temp.png";
  } else if (type == "conductivity") {
    return "/ic_conductivity.png";
  }
  return "/ic_defaul.png";
}
