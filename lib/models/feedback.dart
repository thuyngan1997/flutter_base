import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Feedback extends Equatable {
  final int value;
  final String content;

  Feedback({@required this.value, @required this.content});
  factory Feedback.fromJson(Map<String, dynamic> json) {
    return Feedback(value: json['value'], content: json['content']);
  }
  @override
  List<Object> get props => [
        value,
        content,
      ];
  @override
  String toString() => 'Feedback { value: $value, content: $content }';
}
