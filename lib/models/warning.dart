import 'package:equatable/equatable.dart';

class Warning extends Equatable {
  final int warningId;
  final String parameter;
  final String subject;
  final String message;
  final String time;
  final String icon;
  final int isRead;

  Warning(
      {this.warningId,
      this.parameter,
      this.subject,
      this.message,
      this.time,
      this.icon,
      this.isRead});
  factory Warning.fromJson(Map<String, dynamic> parsedJson) {
    return Warning(
        warningId: parsedJson['warningId'],
        parameter: parsedJson['parameter'],
        subject: parsedJson['subject'],
        message: parsedJson['message'],
        time: parsedJson['time'],
        icon: parsedJson['icon'],
        isRead: parsedJson['isRead']);
  }
  @override
  List<Object> get props =>
      [warningId, parameter, subject, message, time, icon, isRead];

  @override
  String toString() =>
      'Warning { id: $warningId, parameter: $parameter, subject: $subject, message: $message, time: $time, icon: $icon, isRead: $isRead }';
}
