import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class DataParameterMonitoring extends Equatable {
  final String value;
  final DateTime time;

  DataParameterMonitoring({@required this.value, @required this.time});
  factory DataParameterMonitoring.fromJson(Map<String, dynamic> json) {
    DateTime timeParsedDateTime =
        DateFormat("dd-MM-yyyy HH:mm:ss").parse(json['time']);
    return DataParameterMonitoring(
        value: json['value'], time: timeParsedDateTime);
  }
  @override
  List<Object> get props => [
        value,
        time,
      ];
  @override
  String toString() => 'Feedback { value: $value, time: $time }';
}
