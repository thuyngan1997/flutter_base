import 'package:equatable/equatable.dart';

class ParameterMonitoring extends Equatable {
  final String name;
  final int index;
  final String unit;
  final String icon;
  final List<Object> data;
  final String type;
  final int value;

  ParameterMonitoring(
      {this.name,
      this.index,
      this.unit,
      this.icon,
      this.data,
      this.type,
      this.value});

  factory ParameterMonitoring.fromJson(Map<String, dynamic> parsedJson) {
    return ParameterMonitoring(
        name: parsedJson['name'],
        index: parsedJson['index'],
        unit: parsedJson['unit'],
        icon: parsedJson['icon'],
        type: parsedJson['type'],
        value: parsedJson['value'],
        data: parsedJson['data'].map((data) {
          return data;
        }).toList());
  }

  @override
  List<Object> get props => [name, index, data, unit, value, icon];

  @override
  String toString() => 'Parameter { name: $name }';
}
