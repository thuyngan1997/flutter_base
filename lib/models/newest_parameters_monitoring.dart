import 'package:equatable/equatable.dart';

class NewestParameterMonitoring extends Equatable {
  final String name;
  final int index;
  final String unit;
  final String icon;
  final int value;

  NewestParameterMonitoring(
      {this.name, this.index, this.unit, this.icon, this.value});

  factory NewestParameterMonitoring.fromJson(Map<String, dynamic> parsedJson) {
    return NewestParameterMonitoring(
        name: parsedJson['name'],
        index: parsedJson['index'],
        unit: parsedJson['unit'],
        icon: parsedJson['icon'],
        value: parsedJson['value']);
  }

  @override
  List<Object> get props => [name, index, value, unit, icon];

  @override
  String toString() => 'Newest parameter monitoring { name: $name }';
}
