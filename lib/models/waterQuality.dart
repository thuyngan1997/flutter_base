class WaterQuality {
  String id;
  DateTime createAt;
  String parameter;
  int value;
  String quality;

  WaterQuality({
    this.id,
    this.createAt,
    this.parameter,
    this.value,
    this.quality,
  });

  WaterQuality.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parameter = json['parameter'];
    createAt = json['createAt'];
    value = json['value'];
    quality = json['quality'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parameter'] = this.parameter;
    data['createAt'] = this.createAt;
    data['value'] = this.value;
    data['quality'] = this.quality;
    return data;
  }
}

class ListWaterQualityModel {
  List<WaterQuality> waterQualities;

  ListWaterQualityModel({this.waterQualities});

  ListWaterQualityModel.fromJson(Map<String, dynamic> json) {
    if (json['waterQualities'] != null) {
      waterQualities = new List<WaterQuality>();
      json['waterQualities'].forEach((v) {
        waterQualities.add(new WaterQuality.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.waterQualities != null) {
      data['waterQualities'] =
          this.waterQualities.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
