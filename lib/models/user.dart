import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String username;
  final String fullname;
  final int wardId;
  final String identityNo;
  final int roleId;
  final int stationId;
  final int isWarning;
  final int userId;
  final int provinceId;
  final int districtId;
  final int isActive;
  final int isEnable;
  final String address;
  final String phone;
  final int isNew;
  final int countWarningNotRead;
  final String deviceId;
  User(
      {this.identityNo,
      this.stationId,
      this.isWarning,
      this.userId,
      this.provinceId,
      this.roleId,
      this.districtId,
      this.isActive,
      this.isEnable,
      this.address,
      this.username,
      this.fullname,
      this.wardId,
      this.isNew,
      this.phone,
      this.countWarningNotRead,
      this.deviceId});
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        username: json['username'],
        fullname: json['fullname'],
        wardId: json['wardId'],
        roleId: json['roleId'],
        identityNo: json['identityNo'],
        stationId: json['stationId'],
        isWarning: json['isWarning'],
        userId: json['userId'],
        provinceId: json['provinceId'],
        districtId: json['districtId'],
        isActive: json['isActive'],
        isEnable: json['isEnable'],
        address: json['address'],
        phone: json['phone'],
        isNew: json['isNew'],
        countWarningNotRead: json['countWarningNotRead'],
        deviceId: json['deviceId']);
  }
  @override
  List<Object> get props => [
        username,
        fullname,
        wardId,
        identityNo,
        stationId,
        isWarning,
        userId,
        provinceId,
        districtId,
        isActive,
        isEnable,
        address,
        phone,
        isNew,
        deviceId,
        countWarningNotRead
      ];
  @override
  String toString() =>
      'User { username: $username, fullname: $fullname, address: $address}';
}
