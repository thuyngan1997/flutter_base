import 'package:equatable/equatable.dart';

class ResponseDataSuccess extends Equatable {
  final int code;
  final String message;
  final List data;

  ResponseDataSuccess({
    this.code,
    this.message,
    this.data,
  });
  factory ResponseDataSuccess.fromJson(Map<String, dynamic> json) {
    return ResponseDataSuccess(
        code: json['code'],
        message: json['message'],
        data: json['data'].map<Object>((item) {
          return item;
        }).toList());
  }
  @override
  List<Object> get props => [code, message, data];
  @override
  String toString() => 'User { code: ${code.toString()}, message: $message}';
}
