import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class StationGuest extends Equatable {
  final int stationId;
  final String stationName;
  final String deviceId;
  final int wqi;
  final String color;
  final String lattitude;
  final String longitude;
  final String icon;
  final String status;
  final String provinceId;
  final String address;
  final int favouriteStation;

  StationGuest(
      {@required this.stationName,
      @required this.deviceId,
      @required this.wqi,
      @required this.lattitude,
      @required this.longitude,
      @required this.stationId,
      @required this.status,
      @required this.color,
      @required this.icon,
      @required this.provinceId,
      @required this.address,
      @required this.favouriteStation});
  factory StationGuest.fromJson(Map<String, dynamic> parsedJson) {
    return StationGuest(
        stationId: parsedJson['stationId'],
        stationName: parsedJson['stationName'],
        deviceId: parsedJson['deviceId'],
        wqi: parsedJson['wqi'],
        color: parsedJson['color'],
        icon: parsedJson['icon'],
        lattitude: parsedJson['lattitude'],
        status: parsedJson['status'],
        longitude: parsedJson['longitude'],
        provinceId: parsedJson['provinceId'],
        favouriteStation: parsedJson['favouriteStation'],
        address: parsedJson['address']);
  }

  @override
  List<Object> get props => [
        stationId,
        stationName,
        deviceId,
        wqi,
        color,
        icon,
        lattitude,
        status,
        longitude
      ];

  @override
  String toString() => '$stationName';
}
