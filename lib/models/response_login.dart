import 'package:WaterQualityMonitoring/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class DataResponseSuccess extends Equatable {
  final String accessToken;
  final String refreshToken;
  final String tokenType;
  final User user;

  DataResponseSuccess(
      {@required this.accessToken,
      this.refreshToken,
      this.tokenType,
      @required this.user});
  factory DataResponseSuccess.fromJson(Map<String, dynamic> json) {
    User userJson = User.fromJson(json['user']);
    return DataResponseSuccess(
        accessToken: json['accessToken'],
        refreshToken: json['refreshToken'],
        tokenType: json['tokenType'],
        user: userJson);
  }
  @override
  List<Object> get props => [accessToken, refreshToken, tokenType, user];
  @override
  String toString() =>
      'User { accessToken: $accessToken, refreshToken: $refreshToken, tokenType: $tokenType}';
}

class ResponseSuccess extends Equatable {
  final int code;
  final String message;
  final Object data;

  ResponseSuccess({
    this.code,
    this.message,
    this.data,
  });
  factory ResponseSuccess.fromJson(Map<String, dynamic> json) {
    return ResponseSuccess(
        code: json['code'], message: json['message'], data: json['data']);
  }
  @override
  List<Object> get props => [code, message, data];
  @override
  String toString() => 'User { code: ${code.toString()}, message: $message}';
}

class ResponseFailure extends Equatable {
  final DateTime timestamp;
  final int status;
  final String error;
  final String message;

  ResponseFailure({this.message, this.timestamp, this.status, this.error});
  factory ResponseFailure.fromJson(Map<String, dynamic> json) {
    return ResponseFailure(
        status: json['status'],
        message: json['message'],
        error: json['error'],
        timestamp: DateTime(json['timestamp']));
  }
  @override
  List<Object> get props => [timestamp, status, error, message];
  @override
  String toString() =>
      'ResponseFailure { timestamp: $timestamp, message: $message, status: ${status.toString()},error:$error }';
}
