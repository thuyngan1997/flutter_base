import 'package:equatable/equatable.dart';

class ResponsePostMethodSuccess extends Equatable {
  final int code;
  final String message;

  ResponsePostMethodSuccess({
    this.code,
    this.message,
  });
  factory ResponsePostMethodSuccess.fromJson(Map<String, dynamic> json) {
    return ResponsePostMethodSuccess(
        code: json['code'], message: json['message']);
  }
  @override
  List<Object> get props => [code, message];
  @override
  String toString() =>
      'Response success { code: ${code.toString()}, message: $message}';
}

class ResponsePostMethodFailure extends Equatable {
  final String timestamp;
  final int status;
  final String error;
  final String message;

  ResponsePostMethodFailure({
    this.timestamp,
    this.status,
    this.error,
    this.message,
  });
  factory ResponsePostMethodFailure.fromJson(Map<String, dynamic> json) {
    return ResponsePostMethodFailure(
        timestamp: json['timestamp'],
        status: json['status'],
        error: json['error'],
        message: json['message']);
  }
  @override
  List<Object> get props => [timestamp, message, status, timestamp];
  @override
  String toString() =>
      'Response success { code: ${status.toString()}, message: $message},timestamp: $timestamp},error: $error} ';
}
