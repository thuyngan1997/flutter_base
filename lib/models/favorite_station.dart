import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class FavoriteStation extends Equatable {
  final String stationName;
  final String address;
  final int stationId;
  final int wqi;
  final String status;
  final String color;
  final int isDefault;
  final String deviceId;
  final int provinceId;

  FavoriteStation(
      {@required this.stationId,
      @required this.address,
      @required this.stationName,
      @required this.wqi,
      @required this.color,
      @required this.isDefault,
      @required this.status,
      @required this.deviceId,
      @required this.provinceId});
  factory FavoriteStation.fromJson(Map<String, dynamic> parsedJson) {
    return FavoriteStation(
        stationId: parsedJson['stationId'],
        address: parsedJson['address'],
        stationName: parsedJson['stationName'],
        wqi: parsedJson['wqi'],
        color: parsedJson['color'],
        isDefault: parsedJson['isDefault'],
        provinceId: parsedJson['provinceId'],
        deviceId: parsedJson['deviceId'],
        status: parsedJson['status']);
  }

  @override
  List<Object> get props => [
        stationId,
        address,
        stationName,
        wqi,
        color,
        isDefault,
        status,
        deviceId,
        provinceId
      ];

  @override
  String toString() =>
      'Station for guest { stationId: $stationId, address: $address, stationName: $stationName, wqi: $wqi, isDefault: $isDefault}';
}
