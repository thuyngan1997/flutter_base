import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SupportData extends Equatable {
  final String status;
  final double min;
  final double max;
  final String color;
  final String icon;
  final String purpose;

  SupportData({
    @required this.status,
    @required this.min,
    @required this.max,
    @required this.color,
    @required this.icon,
    @required this.purpose,
  });
  factory SupportData.fromJson(Map<String, dynamic> parsedJson) {
    return SupportData(
        status: parsedJson['status'],
        min: parsedJson['min'],
        max: parsedJson['max'],
        color: parsedJson['color'],
        icon: parsedJson['icon'],
        purpose: parsedJson['purpose']);
  }

  @override
  List<Object> get props => [
        status,
        min,
        max,
        color,
        icon,
        purpose,
      ];

  @override
  String toString() => '$status';
}
