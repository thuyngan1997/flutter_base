import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Monitoring extends Equatable {
  final String id;
  final String type;
  final List listData;

  Monitoring({@required this.id, @required this.type, @required this.listData});
  factory Monitoring.fromJson(Map<String, dynamic> parsedJson) {
    return Monitoring(
        id: parsedJson['id'],
        type: parsedJson['type'],
        listData: parsedJson['listData'].map((data) {
          return data;
        }).toList());
  }
  @override
  List<Object> get props => [id, type, listData];
  @override
  String toString() => 'Feedback { id: $id, type: $type, listData: $listData }';
}
