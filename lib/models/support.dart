import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Support extends Equatable {
  final int parameterId;
  final String name;
  final String infomation;
  final String unit;
  final String icon;
  final String type;
  final List<Object> support;

  Support({
    @required this.parameterId,
    @required this.name,
    @required this.infomation,
    @required this.unit,
    @required this.icon,
    @required this.type,
    @required this.support,
  });
  factory Support.fromJson(Map<String, dynamic> parsedJson) {
    return Support(
      parameterId: parsedJson['parameterId'],
      name: parsedJson['name'],
      infomation: parsedJson['infomation'],
      unit: parsedJson['unit'],
      icon: parsedJson['icon'],
      type: parsedJson['type'],
      support: parsedJson['support'].map((support) {
        return support;
      }).toList(),
    );
  }

  @override
  List<Object> get props => [
        parameterId,
        name,
        infomation,
        unit,
        icon,
        type,
        support,
      ];

  @override
  String toString() => '$name';
}
