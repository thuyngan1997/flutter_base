part of 'bottom_navigation_bloc.dart';

abstract class BottomNavigationState extends Equatable {
  const BottomNavigationState();
  @override
  List<Object> get props => [];
}

class CurrentIndexChanged extends BottomNavigationState {
  final int currentIndex;

  CurrentIndexChanged({@required this.currentIndex});

  @override
  String toString() => 'CurrentIndexChanged to $currentIndex';

  @override
  List<Object> get props => [currentIndex];
}

class PageLoading extends BottomNavigationState {
  @override
  String toString() => 'Page loading';
}

class HomePageLoaded extends BottomNavigationState {
  final Widget homePage;

  HomePageLoaded({this.homePage});

  @override
  String toString() => 'Home page loaded';
}

class WarningPageLoaded extends BottomNavigationState {
  final Widget warningPage;
  WarningPageLoaded({this.warningPage});

  @override
  String toString() => 'Warning Page Loaded';
}

class HistoryPageLoaded extends BottomNavigationState {
  final Widget historyPage;
  HistoryPageLoaded({this.historyPage});

  @override
  String toString() => 'History Page Loaded';
}

class AccountPageLoaded extends BottomNavigationState {
  final Widget accountPage;
  AccountPageLoaded({this.accountPage});
  @override
  String toString() => 'Account Page Loaded';
}
