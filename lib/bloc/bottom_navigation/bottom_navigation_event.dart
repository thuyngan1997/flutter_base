part of 'bottom_navigation_bloc.dart';

abstract class BottomNavigationEvent extends Equatable {
  const BottomNavigationEvent();
  @override
  List<Object> get props => [];
}

class TabStarted extends BottomNavigationEvent {
  @override
  String toString() => "TadStarted";
}

class PageTapped extends BottomNavigationEvent {
  final int index;

  const PageTapped({@required this.index});

  @override
  List<Object> get props => [index];

  @override
  String toString() => 'PageTapped: $index';
}
