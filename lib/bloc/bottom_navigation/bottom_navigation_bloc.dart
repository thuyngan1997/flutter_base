import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'bottom_navigation_event.dart';
part 'bottom_navigation_state.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  int currentIndex = 0;

  BottomNavigationBloc() : super(PageLoading());

  // @override
  // BottomNavigationState get initialState => PageLoading();

  @override
  Stream<BottomNavigationState> mapEventToState(
      BottomNavigationEvent event) async* {
    if (event is TabStarted) {
      this.add(PageTapped(index: this.currentIndex));
    }
    if (event is PageTapped) {
      this.currentIndex = event.index;
      yield CurrentIndexChanged(currentIndex: this.currentIndex);
      yield PageLoading();

      if (this.currentIndex == 0) {
        yield HomePageLoaded();
      }
      if (this.currentIndex == 1) {
        yield WarningPageLoaded();
      }
      if (this.currentIndex == 2) {
        yield HistoryPageLoaded();
      }
      if (this.currentIndex == 3) {
        yield AccountPageLoaded();
      }
    }
  }
}
