part of 'warning_screen_bloc.dart';

abstract class WarningState {
  const WarningState();

  @override
  List<Object> get props => [];
}

class WarningInitial extends WarningState {}

class WarningFailure extends WarningState {
  final dynamic error;
  const WarningFailure({this.error});
  @override
  String toString() => 'Warning failure: ${error.toString()}';
}

class WarningSuccess extends WarningState {
  final List<Warning> warnings;
  final bool hasReachedMax;

  const WarningSuccess({
    this.warnings,
    this.hasReachedMax,
  });

  WarningSuccess copyWith({
    List<Warning> warnings,
    bool hasReachedMax,
  }) {
    return WarningSuccess(
      warnings: warnings ?? this.warnings,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [warnings, hasReachedMax];

  @override
  String toString() =>
      'WarningSuccessSuccess { posts: ${warnings.length}, hasReachedMax: $hasReachedMax }';
}

class ReadWarningSuccess extends WarningState {}

class ReadWarningFailure extends WarningState {}
