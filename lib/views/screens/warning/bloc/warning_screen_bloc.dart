import 'dart:convert';
import 'dart:io';
import 'package:WaterQualityMonitoring/common/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:WaterQualityMonitoring/models/models.dart';

part 'warning_screen_event.dart';
part 'warning_screen_state.dart';

class WarningBloc extends Bloc<WarningEvent, WarningState> {
  final http.Client httpClient;

  WarningBloc({@required this.httpClient}) : super(WarningInitial());
  final storage = new FlutterSecureStorage();

  @override
  Stream<Transition<WarningEvent, WarningState>> transformEvents(
    Stream<WarningEvent> events,
    TransitionFunction<WarningEvent, WarningState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<WarningState> mapEventToState(WarningEvent event) async* {
    if (event is WarningFetched) {
      yield WarningInitial();
      try {
        final warnings = await _fetchWarnings(0, 20);
        yield WarningSuccess(warnings: warnings);
        return;
      } catch (error) {
        yield WarningFailure(error: error.message);
      }
    }
    if (event is UnreadWarningItemPressed) {
      try {
        await readWarning(event.warningId);
        yield ReadWarningSuccess();
      } catch (error) {
        yield ReadWarningFailure();
      }
    }
    if (event is WarningRefreshRequested) {
      try {
        final warnings = await _fetchWarnings(0, 20);
        yield WarningSuccess(warnings: warnings);
        return;
      } catch (error) {
        yield WarningFailure(error: error.message);
      }
    }
  }

  Future<List<Warning>> _fetchWarnings(int startIndex, int limit) async {
    final token = await storage.read(key: "token");

    final response = await httpClient.get('$baseUrl/warning',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    print(response.body);
    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      ResponseDataSuccess responseDataSuccess =
          ResponseDataSuccess.fromJson(json.decode(response.body));
      final listWarnings = responseDataSuccess.data;
      return listWarnings.map((warning) {
        return Warning.fromJson(warning);
      }).toList();
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }

  Future<bool> readWarning(int warningId) async {
    final token = await storage.read(key: "token");

    final response = await httpClient.put('$baseUrl/warning/$warningId',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    print(response.body);
    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }
}
