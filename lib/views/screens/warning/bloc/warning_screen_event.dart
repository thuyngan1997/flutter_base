part of 'warning_screen_bloc.dart';

abstract class WarningEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class WarningFetched extends WarningEvent {}

class UnreadWarningItemPressed extends WarningEvent {
  final int warningId;

  UnreadWarningItemPressed({@required this.warningId});

  @override
  List<Object> get props => [warningId];

  @override
  String toString() => 'ReadWarningItemPressed { Warningid: $warningId}';
}

class WarningRefreshRequested extends WarningEvent {}
