import 'package:flutter/material.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../common/constants.dart';

class WarningScreen extends StatefulWidget {
  @override
  _WarningScreenState createState() => _WarningScreenState();
}

class _WarningScreenState extends State<WarningScreen> {
  @override
  void initState() {
    super.initState();
  }

  final storage = new FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(
            TITLE_WARNING_SCREEN,
            style: TextStyle(fontSize: 21),
          ),
          centerTitle: true,
        ),
        extendBodyBehindAppBar: true,
        body: Center(
          child: Text('OK'),
        ));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
