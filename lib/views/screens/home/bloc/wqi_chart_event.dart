part of 'wqi_chart_bloc.dart';

abstract class WQIChartEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class DataParameterChartFetched extends WQIChartEvent {}
