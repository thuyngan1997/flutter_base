import 'dart:convert';
import 'dart:io';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:WaterQualityMonitoring/models/models.dart';

part 'wqi_chart_event.dart';
part 'wqi_chart_state.dart';

class WQIChartBloc extends Bloc<WQIChartEvent, WQIChartState> {
  final http.Client httpClient;

  WQIChartBloc({@required this.httpClient}) : super(DataChartInitial());
  final storage = new FlutterSecureStorage();

  @override
  Stream<WQIChartState> mapEventToState(WQIChartEvent event) async* {
    if (event is DataParameterChartFetched) {
      yield DataChartLoading();
      try {
        final listParameters = await getParametersInThisDay();
        final dataWQI = listParameters
            .where((parameter) => parameter.name == "WQI")
            .toList();
        print("==============");
        print(dataWQI);

        yield WQIChartSuccess(parameterMonitoring: dataWQI[0]);
        return;
      } catch (error) {
        yield DataChartFailure(error: error);
      }
    }
  }

  Future<List<ParameterMonitoring>> getParametersInThisDay() async {
    final date = DateFormat("yyyy-MM-dd").format(DateTime.now());
    final token = await storage.read(key: "token");
    final response = await httpClient.get(
        'http://10.60.158.89:9003/monitorings/v1/wqi/device/water001/historical?servicePath=/DaNang&date=$date',
        headers: {HttpHeaders.authorizationHeader: token});
    var bodyResponse = json.decode(response.body);
    if (response.statusCode == 200) {
      Monitoring data = Monitoring.fromJson(bodyResponse['data']);
      final listParameterMonitoring = data.listData.map((e) {
        ParameterMonitoring parameterMonitoring =
            ParameterMonitoring.fromJson(e);
        return parameterMonitoring;
      }).toList();

      return listParameterMonitoring;
    } else {
      throw Exception(
          'Lỗi: ${response.statusCode}: ${bodyResponse['message']} ');
    }
  }
}
