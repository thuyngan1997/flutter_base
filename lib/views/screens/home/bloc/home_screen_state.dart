part of 'home_screen_bloc.dart';

abstract class HomeScreenState extends Equatable {
  const HomeScreenState();

  @override
  List<Object> get props => [];
}

class GetStationInitial extends HomeScreenState {}

class GetStationFailure extends HomeScreenState {
  final dynamic error;
  const GetStationFailure({this.error});
  @override
  String toString() => 'GetStationFailure : ${error.toString()}';
}

class GetStationSuccess extends HomeScreenState {
  final List<StationGuest> stations;

  const GetStationSuccess({
    @required this.stations,
  });

  @override
  List<Object> get props => [stations];

  @override
  String toString() => 'StationSuccess { Lenght: ${stations.length} }';
}

class AddOrRemoveSuccess extends HomeScreenState {}

class AddOrRemoveFailure extends HomeScreenState {
  final dynamic error;
  const AddOrRemoveFailure({this.error});
  @override
  String toString() => 'AddOrRemoveFailure : ${error.toString()}';
}
