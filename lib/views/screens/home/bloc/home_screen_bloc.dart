import 'dart:convert';
import 'dart:io';
import 'package:WaterQualityMonitoring/common/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:WaterQualityMonitoring/models/models.dart';

part 'home_screen_event.dart';
part 'home_screen_state.dart';

class HomeScreenBloc extends Bloc<HomeScreenEvent, HomeScreenState> {
  final http.Client httpClient;

  HomeScreenBloc({@required this.httpClient}) : super(GetStationInitial());
  final storage = new FlutterSecureStorage();
  final BehaviorSubject<List<StationGuest>> _stations =
      BehaviorSubject<List<StationGuest>>();
  _getStationsForUser() async {
    List<StationGuest> response = await getStationsForUser();
    _stations.sink.add(response);
  }

  HomeScreenBloc homeScreenBloc;
  dispose() {
    _stations.close();
  }

  BehaviorSubject<List<StationGuest>> get subject => _stations;

  @override
  Stream<Transition<HomeScreenEvent, HomeScreenState>> transformEvents(
    Stream<HomeScreenEvent> events,
    TransitionFunction<HomeScreenEvent, HomeScreenState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<HomeScreenState> mapEventToState(HomeScreenEvent event) async* {
    if (event is PrivateStationsFetched) {
      yield GetStationInitial();
      try {
        final stations = await getStationsForUser();
        yield GetStationSuccess(stations: stations);
        return;
      } catch (_) {
        yield GetStationFailure(error: _);
      }
    }
    if (event is AddOrRemoveFavoriteStation) {
      try {
        await removeFavoriteStation(event.stationId);
        yield AddOrRemoveSuccess();
        this.add(PrivateStationsFetched());
        return;
      } catch (error) {
        yield AddOrRemoveFailure(error: error);
      }
    }
    if (event is HomeRefreshRequested) {
      try {
        final stations = await getStationsForUser();
        yield GetStationSuccess(stations: stations);
        return;
      } catch (error) {
        yield GetStationFailure(error: error.message);
      }
    }
  }

  Future<List<StationGuest>> getStationsForUser() async {
    final token = await storage.read(key: 'token');

    print(token);
    final response = await httpClient.get(
      '$baseUrl/wqi/station/user-roles-mobile',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: "Bearer $token",
      },
    );

    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      ResponseDataSuccess responseDataSuccess =
          ResponseDataSuccess.fromJson(json.decode(response.body));
      final listStations = responseDataSuccess.data;
      return listStations.map((station) {
        return StationGuest.fromJson(station);
      }).toList();
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }

  Future<bool> removeFavoriteStation(int stationId) async {
    final token = await storage.read(key: "token");
    final response = await httpClient.put(
        '$baseUrl/favourite/${stationId.toString()}',
        headers: {HttpHeaders.authorizationHeader: token});
    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }
}
