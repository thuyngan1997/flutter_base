part of 'wqi_chart_bloc.dart';

abstract class WQIChartState extends Equatable {
  const WQIChartState();

  @override
  List<Object> get props => [];
}

class DataChartInitial extends WQIChartState {}

class DataChartLoading extends WQIChartState {}

class DataChartFailure extends WQIChartState {
  final dynamic error;
  const DataChartFailure({this.error});
  @override
  String toString() => 'Home failure: ${error.toString()}';
}

class WQIChartSuccess extends WQIChartState {
  final ParameterMonitoring parameterMonitoring;
  const WQIChartSuccess({@required this.parameterMonitoring});
  @override
  String toString() => 'WQIChartSuccess: ${parameterMonitoring.toString()}';
}
