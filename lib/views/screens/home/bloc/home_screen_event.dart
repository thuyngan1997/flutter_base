part of 'home_screen_bloc.dart';

abstract class HomeScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PrivateStationsFetched extends HomeScreenEvent {}

class AddOrRemoveFavoriteStation extends HomeScreenEvent {
  final int stationId;
  AddOrRemoveFavoriteStation({@required this.stationId});
  @override
  List<Object> get props => [stationId];

  @override
  String toString() => 'AddOrRemoveFavoriteStation { stationId: $stationId}';
}

class HomeRefreshRequested extends HomeScreenEvent {}
