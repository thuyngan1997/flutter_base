import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:WaterQualityMonitoring/common/constants.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            TITLE_PROFILE_SCREEN,
            style: TextStyle(fontSize: 21),
          ),
          centerTitle: true,
        ),
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        body: Center(
          child: Text('OK'),
        ));
  }
}
