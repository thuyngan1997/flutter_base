import 'dart:async';
import 'dart:ui';

import 'package:WaterQualityMonitoring/views/screens/favorite_station/bloc/favorite_station_screen_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:WaterQualityMonitoring/common/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

import 'bloc/favorite_station_screen_bloc.dart';

class StackFavoriteStationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(initialRoute: '/', routes: <String, WidgetBuilder>{
      '/': (context) => FavoriteStationScreen(),
    });
  }
}

class FavoriteStationScreen extends StatefulWidget {
  @override
  _FavoriteStationScreenState createState() => _FavoriteStationScreenState();
}

Completer<void> _refreshCompleter;

class _FavoriteStationScreenState extends State<FavoriteStationScreen> {
  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            TITLE_FOLLOWED_PLACE_SCREEN,
            style: TextStyle(fontSize: 22),
          ),
        ),
        extendBodyBehindAppBar: true,
        body: Center(
          child: Text('OK'),
        ));
  }
}
