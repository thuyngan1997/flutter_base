part of 'favorite_station_screen_bloc.dart';

abstract class FavoriteStationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FavoriteStationsFetched extends FavoriteStationEvent {}

class RemoveFavoriteStation extends FavoriteStationEvent {
  final int stationId;

  RemoveFavoriteStation({@required this.stationId});

  @override
  List<Object> get props => [stationId];

  @override
  String toString() => 'RemoveFavoriteStation { stationId: $stationId}';
}

class FavoriteStationsRefreshRequested extends FavoriteStationEvent {}
