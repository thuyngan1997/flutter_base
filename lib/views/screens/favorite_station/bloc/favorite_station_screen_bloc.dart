import 'dart:convert';
import 'dart:io';
import 'package:WaterQualityMonitoring/common/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:WaterQualityMonitoring/models/models.dart';

import '../../../../common/constants.dart';

part 'favorite_station_screen_event.dart';
part 'favorite_station_screen_state.dart';

class FavoriteStationBloc
    extends Bloc<FavoriteStationEvent, FavoriteStationState> {
  final http.Client httpClient;

  FavoriteStationBloc({@required this.httpClient})
      : super(FavoriteStationInitial());
  final storage = new FlutterSecureStorage();

  @override
  Stream<Transition<FavoriteStationEvent, FavoriteStationState>>
      transformEvents(
    Stream<FavoriteStationEvent> events,
    TransitionFunction<FavoriteStationEvent, FavoriteStationState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<FavoriteStationState> mapEventToState(
      FavoriteStationEvent event) async* {
    if (event is FavoriteStationsFetched) {
      try {
        final favoriteStations = await getFavoriteStations();
        yield FavoriteStationSuccess(favoriteStations: favoriteStations);
      } catch (error) {
        yield FavoriteStationFailure(error: error);
      }
    }
    if (event is RemoveFavoriteStation) {
      try {
        await removeFavoriteStation(event.stationId);
        yield RemoveFavoriteStationSuccess();
        this.add(FavoriteStationsFetched());
      } catch (error) {
        yield RemoveFavoriteStationFailure(error: error.message);
      }
    }
    if (event is FavoriteStationsRefreshRequested) {
      try {
        final favoriteStations = await getFavoriteStations();
        yield FavoriteStationSuccess(favoriteStations: favoriteStations);
      } catch (error) {
        yield FavoriteStationFailure(error: error.message);
      }
    }
  }

  Future<List<FavoriteStation>> getFavoriteStations() async {
    final token = await storage.read(key: "token");

    final response = await httpClient.get('$baseUrl/favourite', headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    });
    print(response.body);
    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      ResponseDataSuccess responseDataSuccess =
          ResponseDataSuccess.fromJson(json.decode(response.body));
      final listFavoriteStations = responseDataSuccess.data;
      return listFavoriteStations.map((station) {
        return FavoriteStation.fromJson(station);
      }).toList();
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }

  Future<bool> removeFavoriteStation(int stationId) async {
    final token = await storage.read(key: "token");
    final response = await httpClient.put(
        '$baseUrl/favourite/${stationId.toString()}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    print('$baseUrl/favourite/${stationId.toString()}');
    var data = json.decode(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Lỗi: ${response.statusCode}: ${data['message']} ');
    }
  }
}
