part of 'favorite_station_screen_bloc.dart';

abstract class FavoriteStationState {
  const FavoriteStationState();

  @override
  List<Object> get props => [];
}

class FavoriteStationInitial extends FavoriteStationState {}

class FavoriteStationFailure extends FavoriteStationState {
  final dynamic error;
  const FavoriteStationFailure({this.error});
  @override
  String toString() => 'FavoriteStationFailure: ${error.toString()}';
}

class FavoriteStationSuccess extends FavoriteStationState {
  final List<FavoriteStation> favoriteStations;

  const FavoriteStationSuccess({
    this.favoriteStations,
  });

  FavoriteStationSuccess copyWith({
    List<FavoriteStation> favoriteStations,
  }) {
    return FavoriteStationSuccess(
      favoriteStations: favoriteStations,
    );
  }

  @override
  List<Object> get props => [favoriteStations];

  @override
  String toString() =>
      'FavoriteStationSuccess { posts: ${favoriteStations.toString()} }';
}

class RemoveFavoriteStationSuccess extends FavoriteStationState {}

class RemoveFavoriteStationFailure extends FavoriteStationState {
  final dynamic error;
  const RemoveFavoriteStationFailure({this.error});
  @override
  String toString() => 'FavoriteStationFailure: ${error.toString()}';
}
