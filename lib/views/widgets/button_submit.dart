import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonSubmit extends StatelessWidget {
  ButtonSubmit({@required this.onPressed, @required this.text});
  final Function onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 30, 0, 10),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 54.0,
          child: RawMaterialButton(
            onPressed: this.onPressed,
            fillColor: Color(0xff3CD2E4),
            child: Text(
              this.text,
              style: TextStyle(
                  color: Color(0xff042D66),
                  fontSize: 20,
                  fontWeight: FontWeight.w600),
            ),
            shape: StadiumBorder(),
          ),
        ));
  }
}
