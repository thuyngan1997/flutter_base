import 'package:flutter/cupertino.dart';

class Router {
  BuildContext context;
  Function route;

  Router({this.context, this.route});

  static void navigateToPage(context, route) {
    Future.delayed(Duration.zero, () {
      Navigator.push(context, CupertinoPageRoute(builder: (context) => route));
    });
  }
}
