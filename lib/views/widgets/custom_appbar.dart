import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppbar({this.onActionPressed, this.actionIcon, @required this.title})
      : preferredSize = Size.fromHeight(kToolbarHeight);
  final Function onActionPressed;
  final String title;
  final dynamic actionIcon;
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: Align(
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(fontSize: 22),
        ),
      ),
      actions: [
        actionIcon != null
            ? IconButton(
                padding: EdgeInsets.only(right: 10, top: 5),
                icon: actionIcon,
                onPressed: onActionPressed,
              )
            : null
      ],
    );
  }
}
