import 'package:table_calendar/table_calendar.dart';
import 'package:flutter/material.dart';

class CalendarComponent extends StatefulWidget {
  CalendarComponent({this.onSelectedDay});
  final Function onSelectedDay;
  @override
  _CalendarComponentState createState() => _CalendarComponentState();
}

class _CalendarComponentState extends State<CalendarComponent> {
  CalendarController calendarController;
  String headerDate;

  @override
  void initState() {
    super.initState();
    calendarController = CalendarController();
    headerDate = DateTime.now().day.toString();
  }

  @override
  void dispose() {
    calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TableCalendar(
            headerVisible: false,
            startingDayOfWeek: StartingDayOfWeek.monday,
            initialCalendarFormat: CalendarFormat.week,
            headerStyle: HeaderStyle(
                centerHeaderTitle: true, formatButtonShowsNext: false),
            onDaySelected: widget.onSelectedDay,
            builders: CalendarBuilders(
              selectedDayBuilder: (context, date, events) => Container(
                  margin: const EdgeInsets.all(4.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color(0xff8FDAFF),
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Text(
                    date.day.toString(),
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  )),
              todayDayBuilder: (context, date, events) => Container(
                  margin: const EdgeInsets.all(4.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Text(
                    date.day.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  )),
              dayBuilder: (context, date, events) => Container(
                  margin: const EdgeInsets.all(4.0),
                  alignment: Alignment.center,
                  child: Text(
                    date.day.toString(),
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  )),
            ),
            calendarController: calendarController)
      ],
    );
  }
}

class CustomCalendarHeader extends StatelessWidget {
  final String headerDate;

  const CustomCalendarHeader({
    Key key,
    @required this.headerDate,
  })  : assert(headerDate != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Center(
        child: Text(
          headerDate,
          style: TextStyle(fontSize: 20.0),
        ),
      ),
    );
  }
}
