import 'package:WaterQualityMonitoring/common/constants.dart';
import 'package:WaterQualityMonitoring/views/screens/favorite_station/bloc/favorite_station_screen_bloc.dart';
import 'package:WaterQualityMonitoring/views/screens/screens.dart';
import 'package:WaterQualityMonitoring/bloc/blocs.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_bloc/flutter_bloc.dart';

class AppScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final BottomNavigationBloc bottomNavigationBloc =
        BlocProvider.of<BottomNavigationBloc>(context);
    return Scaffold(
      body: BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
        builder: (BuildContext context, BottomNavigationState state) {
          if (state is PageLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is HomePageLoaded) {
            return HomeScreen();
          }
          if (state is WarningPageLoaded) {
            return WarningScreen();
          }
          if (state is HistoryPageLoaded) {
            return FavoriteStationScreen();
          }
          if (state is AccountPageLoaded) {
            return AccountScreen();
          }
          return Container();
        },
      ),
      bottomNavigationBar:
          BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
              builder: (BuildContext context, BottomNavigationState state) {
        return BottomNavigationBar(
          showSelectedLabels: false,
          currentIndex: bottomNavigationBloc.currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/ic_home.png',
                  height: 25,
                  width: 25,
                ),
                title: Text(
                  'Trang chủ',
                  style: TextStyle(color: Color(0xff7992A3)),
                ),
                activeIcon: Image.asset(
                  'assets/images/ic_home_active.png',
                  height: 25,
                  width: 25,
                )),
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/ic_warning.png',
                  height: 25,
                  width: 25,
                ),
                title: Text(
                  'Cảnh báo',
                  style: TextStyle(color: Color(0xff7992A3)),
                ),
                activeIcon: Image.asset(
                  'assets/images/ic_warning_active.png',
                  height: 25,
                  width: 25,
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  FeatherIcons.heart,
                  size: 25,
                  color: COLOR_LABEL,
                ),
                title: Text(
                  'Lịch sử',
                  style: TextStyle(color: Color(0xff7992A3)),
                ),
                activeIcon: Icon(
                  FeatherIcons.heart,
                  size: 25,
                  color: Color(0xff5DE5F5),
                )),
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/ic_account.png',
                  height: 25,
                  width: 25,
                ),
                title: Text(
                  'Tài khoản',
                  style: TextStyle(color: Color(0xff7992A3)),
                ),
                activeIcon: Image.asset(
                  'assets/images/ic_account_active.png',
                  height: 25,
                  width: 25,
                )),
          ],
          onTap: (index) => bottomNavigationBloc.add(PageTapped(index: index)),
        );
      }),
    );
  }
}
