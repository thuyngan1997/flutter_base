import 'package:WaterQualityMonitoring/app.dart';
import 'package:WaterQualityMonitoring/bloc/blocs.dart';
import 'package:WaterQualityMonitoring/views/screens/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _AppState();
}

class _AppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: BlocProvider<BottomNavigationBloc>(
      create: (context) => BottomNavigationBloc()..add(TabStarted()),
      child: AppScreen(),
    ));
  }
}
