# wqm-app

WQM_Application
  

## Getting started
- Cài đặt môi trường: https://flutter.dev/docs/get-started/install
- Install package: flutter pub get
(Package được thêm vào file pubspec.yaml, sau đó sử dụng lệnh flutter pub get)
- Run: flutter run


## SET PROXY
// download flutter git
git config --global http.proxy 10.61.11.42:3128
git clone https://github.com/flutter/flutter.git -b stable

//set proxy flutter run doctor
export NO_PROXY=10.61.11.42:3128
flutter doctor

//setproxy download androidsdk command line
sdkmanager “system-images;android-28;default;x86_64” --no_https --proxy=http --proxy_host=10.61.11.42 --proxy_port=3128
sdkmanager “platform-tools” --proxy=http --proxy_host=10.61.11.42 --proxy_port=3128
sdkmanager “build-tools;28.0.3” --proxy=http --proxy_host=10.61.11.42 --proxy_port=3128
sdkmanager “platforms;android-28” --proxy=http --proxy_host=10.61.11.42 --proxy_port=3128
sdkmanager emulator --proxy=http --proxy_host=10.61.11.42 --proxy_port=3128



